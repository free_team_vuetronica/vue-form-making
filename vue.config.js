const TerserPlugin = require('terser-webpack-plugin')
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssertsPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = {
  productionSourceMap: false,
  publicPath: './',
  configureWebpack: config => {
    let plugins = [
      new TerserPlugin({
        terserOptions: {
          compress: {
            warnings: false,
            drop_debugger: false,
            drop_console: true,
          },
        },
        sourceMap: false,
        parallel: true,
      }),
      // new UglifyJsPlugin({
      //   cache: true,
      //   parallel: true,
      //   sourceMap: true,
      //   uglifyOptions: {
      //     warnings: false,
      //     output: {
      //       comments: false,
      //       beautify: false
      //     },
      //     compress: {
      //       drop_console: true,
      //       collapse_vars: true,
      //       reduce_vars: true,
      //     }
      //   }
      // }),
      new OptimizeCSSAssertsPlugin({})
    ]
    if (process.env.NODE_ENV !== 'development') {
      config.plugins = [...config.plugins, ...plugins]
    }
  }
}