import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'vue-vertical-table/dist/VerticalTable.css'

import 'element-ui/lib/theme-chalk/index.css'
import '@fortawesome/fontawesome-free/scss/fontawesome.scss'

// import 'form-making/dist/FormMaking.css'
// import FormMaking from 'form-making'
import FormMaking from './index'
Vue.use(FormMaking)

Vue.config.productionTip = false

Vue.use(ElementUI, {
  size: 'small'
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
