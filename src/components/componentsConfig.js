export const basicComponents = [
  {
    type: "input",
    name: "單行文字",
    icon: "fa-font",
    options: {
      width: "100%",
      defaultValue: "",
      required: false,
      dataType: "string",
      pattern: "",
      placeholder: "",
      disabled: false
    }
  },
  {
    type: "textarea",
    name: "多行文字",
    icon: "fa-align-justify",
    options: {
      width: "100%",
      defaultValue: "",
      required: false,
      disabled: false,
      pattern: "",
      placeholder: ""
    }
  },
  {
    type: "number",
    name: "數字",
    icon: "fa-sort-numeric-down-alt",
    options: {
      width: "",
      required: false,
      defaultValue: 0,
      min: "",
      max: "",
      step: 1,
      disabled: false,
      controlsPosition: ""
    }
  },
  {
    type: "text",
    name: "文字",
    icon: "fa-paragraph",
    options: {
      defaultValue: "一段文字",
      customClass: ""
    }
  },
  {
    type: "radio",
    name: "單選",
    icon: "fa-dot-circle",
    options: {
      inline: false,
      defaultValue: "",
      showLabel: false,
      options: [
        {
          value: "選項1",
          label: "選項1"
        },
        {
          value: "選項2",
          label: "選項2"
        },
        {
          value: "選項3",
          label: "選項3"
        }
      ],
      required: false,
      width: "",
      remote: false,
      remoteOptions: [],
      props: {
        value: "value",
        label: "label"
      },
      remoteFunc: "",
      disabled: false
    }
  },
  {
    type: "checkbox",
    name: "多選",
    icon: "fa-check-double",
    options: {
      inline: false,
      defaultValue: [],
      showLabel: false,
      options: [
        {
          value: "選項1"
        },
        {
          value: "選項2"
        },
        {
          value: "選項3"
        }
      ],
      required: false,
      width: "",
      remote: false,
      remoteOptions: [],
      props: {
        value: "value",
        label: "label"
      },
      remoteFunc: "",
      disabled: false
    }
  },
  {
    type: "time",
    name: "時間選擇器",
    icon: "fa-clock",
    options: {
      defaultValue: "",
      readonly: false,
      disabled: false,
      editable: true,
      clearable: true,
      placeholder: "",
      startPlaceholder: "",
      endPlaceholder: "",
      isRange: false,
      arrowControl: true,
      format: "",
      required: false,
      width: ""
    }
  },
  {
    type: "date",
    name: "日期選擇器",
    icon: "fa-calendar-week",
    options: {
      defaultValue: "",
      readonly: false,
      disabled: false,
      editable: true,
      clearable: true,
      placeholder: "",
      startPlaceholder: "",
      endPlaceholder: "",
      type: "date",
      format: "",
      timestamp: false,
      required: false,
      width: ""
    }
  },
  {
    type: "rate",
    name: "評分",
    icon: "icon-icon-test",
    options: {
      defaultValue: null,
      max: 5,
      disabled: false,
      allowHalf: false,
      required: false
    }
  },
  {
    type: "color",
    name: "顏色選擇器",
    icon: "icon-color",
    options: {
      defaultValue: "",
      disabled: false,
      showAlpha: false,
      required: false
    }
  },
  {
    type: "select",
    name: "下拉選單",
    icon: "fa-check-square",
    options: {
      defaultValue: "",
      multiple: false,
      disabled: false,
      clearable: false,
      placeholder: "",
      required: false,
      showLabel: false,
      width: "",
      options: [
        {
          value: "下拉框1"
        },
        {
          value: "下拉框2"
        },
        {
          value: "下拉框3"
        }
      ],
      remote: false,
      filterable: false,
      remoteOptions: [],
      props: {
        value: "value",
        label: "label"
      },
      remoteFunc: ""
    }
  },
  // {
  //   type: 'switch',
  //   name: '開關',
  //   icon: 'icon-switch',
  //   options: {
  //     defaultValue: false,
  //     required: false,
  //     disabled: false,
  //   }
  // },
  // {
  //   type: 'slider',
  //   name: '滑塊',
  //   icon: 'icon-slider',
  //   options: {
  //     defaultValue: 0,
  //     disabled: false,
  //     required: false,
  //     min: 0,
  //     max: 100,
  //     step: 1,
  //     showInput: false,
  //     range: false,
  //     width: ''
  //   }
  // },
  {
    type: "fileupload",
    name: "文件",
    icon: "fa-file-upload",
    options: {
      defaultValue: [],
      tips: "",
      width: "",
      disabled: false,
      length: 1,
      multiple: false,
      isDelete: false,
      min: 0,
      isEdit: false,
      action: "https://dataknit.api.ezclass.tw/api/v1/uploads/uploadFile"
    }
  },
  {
    type: "imgupload",
    name: "圖片",
    icon: "fa-image",
    options: {
      defaultValue: [],
      size: {
        width: 100,
        height: 100
      },
      width: "",
      disabled: false,
      length: 1,
      isDelete: false,
      multiple: false,
      min: 0,
      isEdit: false,
      action: "https://dataknit.api.ezclass.tw/api/v1/uploads/uploadFile"
    }
  },
  {
    type: "divider",
    name: "分隔線",
    icon: "fa-grip-lines",
    options: {
      placeholder: "",
      contentPosition: "left"
    }
  },
  {
    type: "vtable",
    name: "表格",
    icon: "fa-table",
    options: {
      // disabled: false,
      width: "600px",
      // defaultValue: [],
      defaultValue: [
        {
          id: 0,
          key: "row1",
          children: [
            {
              isEdit: true,
              type: "input",
              key: "列1",
              value: ""
            },
            {
              isEdit: true,
              type: "input",
              key: "列2",
              value: ""
            }
          ]
        },
        {
          id: 1,
          key: "row2",
          children: [
            {
              isEdit: true,
              type: "input",
              key: "列1",
              value: ""
            }
          ]
        }
      ]
    }
  },
  {
    type: "crosstable",
    name: "交叉表格",
    icon: "fa-table",
    options: {
      // disabled: false,
      width: "600px",
      // defaultValue: [],
      defaultValue: [
        {
          id: 0,
          title: "1機",
          children: [
            {
              name: "電壓",
              value: "",
              isEdit: true,
              type: "input"
            },
            {
              name: "電流",
              value: "",
              isEdit: true,
              type: "input"
            },
            {
              name: "是否更換",
              value: "",
              isEdit: true,
              type: "input"
            },
            {
              name: "耗材",
              value: "",
              isEdit: true,
              type: "input"
            }
          ]
        },
        {
          id: 1,
          title: "2機",
          children: [
            {
              name: "電壓",
              value: "",
              isEdit: true,
              type: "input"
            },
            {
              name: "電流",
              value: "",
              isEdit: true,
              type: "input"
            },
            {
              name: "是否更換",
              value: "",
              isEdit: true,
              type: "input"
            },
            {
              name: "耗材",
              value: "",
              isEdit: true,
              type: "input"
            }
          ]
        }
      ]
    }
  }
];

export const advanceComponents = [
  // {
  //   type: 'blank',
  //   name: '自訂',
  //   icon: 'icon-ic',
  //   options: {
  //     defaultType: 'String'
  //   }
  // },
  // {
  //   type: 'imgupload',
  //   name: '圖片',
  //   icon: 'icon-tupian',
  //   options: {
  //     defaultValue: [],
  //     size: {
  //       width: 100,
  //       height: 100,
  //     },
  //     width: '',
  //     tokenFunc: 'funcGetToken',
  //     token: '',
  //     domain: 'http://pfp81ptt6.bkt.clouddn.com/',
  //     disabled: false,
  //     length: 8,
  //     multiple: false,
  //     isQiniu: false,
  //     isDelete: false,
  //     min: 0,
  //     isEdit: false,
  //     action: 'https://jsonplaceholder.typicode.com/photos/'
  //   }
  // },
  // {
  //   type: 'editor',
  //   name: '編輯器',
  //   icon: 'icon-fuwenbenkuang',
  //   options: {
  //     defaultValue: '',
  //     width: ''
  //   }
  // },
  // {
  //   type: 'cascader',
  //   name: '關聯選擇器',
  //   icon: 'icon-jilianxuanze',
  //   options: {
  //     defaultValue: [],
  //     width: '',
  //     placeholder: '',
  //     disabled: false,
  //     clearable: false,
  //     remote: true,
  //     remoteOptions: [],
  //     props: {
  //       value: 'value',
  //       label: 'label',
  //       children: 'children'
  //     },
  //     remoteFunc: ''
  //   }
  // }
];

export const layoutComponents = [
  {
    type: "grid",
    name: "欄",
    icon: "fa-grip-vertical",
    columns: [
      {
        span: 12,
        list: []
      },
      {
        span: 12,
        list: []
      }
    ],
    options: {
      gutter: 0,
      justify: "start",
      align: "top"
    }
  }
];
